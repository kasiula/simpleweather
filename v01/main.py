#!/usr/bin/python
# -*- coding: utf-8 -*-
import httplib
import urllib2
import json
import wx
import os
from pathcreator import PathCreator


class SimpleWeather(wx.Frame):
    def __init__(self, parent, title):
        super(SimpleWeather, self).__init__(parent, title=title, size=(190, 180))

        self.__API_KEY = '0dc0c023864f74ba760873b7ddecf6c8'
        self.pc = PathCreator()

        self.initUI()
        self.refresh()
        self.Centre()
        self.Show()

    def getWeatherCondition(self, city):
        url = "http://api.openweathermap.org/data/2.5/forecast/city?q=%s&APPID=%s" % (city, self.__API_KEY)
        try:
            request = urllib2.Request(url)
            response = urllib2.urlopen(request)
        except urllib2.HTTPError, e:
            info = wx.MessageBox(
                u"Nie można pobrać aktualnej prognozy\npogody!\n\nSprawdź swoje połączenie internetowe.", u"Błąd",
                wx.OK | wx.ICON_ERROR)
            self.setNoWeatherInfo()
            print e
            return None
        except urllib2.URLError, e:
            info = wx.MessageBox(
                u"Nie można pobrać aktualnej prognozy\npogody!\n\nSprawdź swoje połączenie internetowe.", u"Błąd",
                wx.OK | wx.ICON_ERROR)
            self.setNoWeatherInfo()
            print e
            return None
        except httplib.HTTPException, e:
            info = wx.MessageBox(
                u"Nie można pobrać aktualnej prognozy\npogody!\n\nSprawdź swoje połączenie internetowe.", u"Błąd",
                wx.OK | wx.ICON_ERROR)
            self.setNoWeatherInfo()
            print e
            return None
        except Exception, e:
            info = wx.MessageBox(u"Nie można pobrać aktualnej\nprognozy pogody!", u"Błąd", wx.OK | wx.ICON_ERROR)
            self.setNoWeatherInfo()
            print e
            return None
        return response.read()

    def getTempInCelcius(self, weatherCondition):
        return round(weatherCondition['list'][0]['main']['temp'] - 273.15)

    def getTempInKelwins(self, weatherCondition):
        return round(weatherCondition['list'][0]['main']['temp'])

    def getSkyDescriptionPL(self, weatherCondition):
        weatherID = weatherCondition['list'][0]['weather'][0]['id']

    def getSkyIcon(self, weatherCondition):
        iconUrl = "http://openweathermap.org/img/w/"
        iconName = weatherCondition['list'][0]['weather'][0]['icon']
        iconUrl += iconName
        iconUrl += ".png"
        imgData = urllib2.urlopen(iconUrl).read()
        weatherImg = open(self.pc.create_image_path('weather.png'), 'wb')
        weatherImg.write(imgData)
        weatherImg.close()

    def connectActionsWithEvents(self):
        self.Bind(wx.EVT_ICONIZE, self.onIconify)
        self.tbicon.Bind(wx.EVT_TASKBAR_LEFT_DCLICK, self.onTaskBarActivate)
        self.Bind(wx.EVT_MENU, self.onExit, self.exitItem)
        self.Bind(wx.EVT_MENU, self.onRefresh, self.refreshItem)
        self.Bind(wx.EVT_MENU, self.onInfo, self.infoItem)

    def onTaskBarActivate(self, evt):
        if self.IsIconized():
            self.Iconize(False)
            self.Show()
            self.Raise()
            self.tbicon.RemoveIcon()
            self.refresh()

    def onIconify(self, evt):
        if evt.Iconized():
            self.Iconize(True)
            self.Hide()
            self.tbicon.SetIcon(self.favicon)

    def onExit(self, event):
        exitDialog = wx.MessageDialog(self, u'Na pewno zakończyć program? \n', u'Wyjście', wx.YES_NO)
        if exitDialog.ShowModal() == wx.ID_YES:
            self.Close(True)

    def onRefresh(self, event):
        self.refresh()

    def refresh(self):
        try:
            weather = json.loads(self.getWeatherCondition("Lublin"))

            if weather != None:
                currentTemp = self.getTempInCelcius(weather)
                currentTemp = str(currentTemp) + " *C"
                self.setCurrentTemp(currentTemp)
                self.getSkyIcon(weather)
        except Exception, e:
            self.setNoWeatherInfo()
            print e

    def onInfo(self, event):
        aboutInfo = 'Prosta pogodynka w Pythonie.\n\nAutor: Katarzyna Mazur\nWersja: 0.1'
        aboutBox = wx.MessageDialog(self, message=aboutInfo, caption='SimpleWeather v0.1',
                                    style=wx.ICON_INFORMATION | wx.STAY_ON_TOP | wx.OK)
        if aboutBox.ShowModal() == wx.ID_OK:
            aboutBox.Destroy()

    def createMenus(self):
        self.menubar = wx.MenuBar()

        self.weatherMenu = wx.Menu()
        self.menubar.Append(self.weatherMenu, '&Pogoda')
        self.refreshItem = wx.MenuItem(self.weatherMenu, wx.NewId(), u'&Odśwież\tCTRL+R')
        self.refreshItem.SetBitmap(wx.Bitmap(self.pc.create_image_path('refresh.png')))
        self.weatherMenu.AppendItem(self.refreshItem)
        self.weatherMenu.AppendSeparator()
        self.exitItem = wx.MenuItem(self.weatherMenu, wx.NewId(), u'&Zakończ\tCTRL+Q')
        self.exitItem.SetBitmap(wx.Bitmap(self.pc.create_image_path('exit.png')))
        self.weatherMenu.AppendItem(self.exitItem)

        self.infoMenu = wx.Menu()
        self.menubar.Append(self.infoMenu, '&Info')
        self.infoItem = wx.MenuItem(self.infoMenu, wx.NewId(), u'&O programie ...\tCTRL+I')
        self.infoItem.SetBitmap(wx.Bitmap(self.pc.create_image_path('about_me.png')))
        self.infoMenu.AppendItem(self.infoItem)

        self.SetMenuBar(self.menubar)

    def setSkyIcon(self, icon):
        self.weatherIcon.SetBitmap(wx.Bitmap(self.pc.create_image_path(icon)))

    def setCurrentTemp(self, temp):
        self.temperatureLabelDyn.SetLabel(temp)

    def setSkyDescriptionPL(self, description):
        pass

    def setNoWeatherInfo(self):
        self.setSkyIcon(self.pc.create_image_path('no-weather.png'))
        self.setCurrentTemp('')
        self.setSkyDescriptionPL('')

    def initUI(self):

        self.panel = wx.Panel(self)

        self.favicon = wx.Icon(self.pc.create_image_path('sun.ico'), wx.BITMAP_TYPE_ANY, 16, 16)
        self.SetIcon(self.favicon)
        self.tbicon = wx.TaskBarIcon()

        vbox = wx.BoxSizer(wx.VERTICAL)

        hbox1 = wx.BoxSizer(wx.HORIZONTAL)
        st1 = wx.StaticText(self.panel, label=u'Miejscowość:')
        hbox1.Add(st1, flag=wx.RIGHT, border=40)
        cityLabel = wx.StaticText(self.panel, label=u'Lublin')
        hbox1.Add(cityLabel, proportion=1)
        vbox.Add(hbox1, flag=wx.EXPAND | wx.LEFT | wx.RIGHT | wx.TOP, border=8)
        vbox.Add((-1, 1))

        hbox2 = wx.BoxSizer(wx.HORIZONTAL)
        temperatureLabelStat = wx.StaticText(self.panel, label='Temperatura:')
        hbox2.Add(temperatureLabelStat, flag=wx.RIGHT, border=40)
        self.temperatureLabelDyn = wx.StaticText(self.panel, label=u'')
        hbox2.Add(self.temperatureLabelDyn, proportion=1)
        vbox.Add(hbox2, flag=wx.EXPAND | wx.LEFT | wx.RIGHT | wx.TOP, border=8)
        vbox.Add((-1, 1))

        hbox4 = wx.BoxSizer(wx.HORIZONTAL)
        skyConditionLabelStat = wx.StaticText(self.panel, label='Niebo:')
        hbox4.Add(skyConditionLabelStat, flag=wx.RIGHT, border=55)
        self.skyConditionLabelDyn = wx.StaticText(self.panel, label=u'')
        hbox4.Add(self.skyConditionLabelDyn, proportion=1)
        vbox.Add(hbox4, flag=wx.EXPAND | wx.LEFT | wx.RIGHT | wx.TOP, border=8)
        vbox.Add((-1, 10))

        hbox3 = wx.BoxSizer(wx.HORIZONTAL)
        self.weatherIcon = wx.StaticBitmap(self.panel, bitmap=wx.Bitmap(self.pc.create_image_path('weather.png')))
        hbox3.Add(self.weatherIcon, flag=wx.ALIGN_CENTER | wx.CENTER | wx.TOP, border=0)
        vbox.Add(hbox3, flag=wx.ALIGN_CENTER | wx.CENTER | wx.TOP, border=0)

        self.panel.SetSizer(vbox)

        self.createMenus()
        self.connectActionsWithEvents()
        self.SetMaxSize([190, 180])
        self.SetMinSize([190, 180])


if __name__ == '__main__':
    app = wx.App()
    SimpleWeather(None, title='SimpleWeather')
    app.MainLoop()
