# !/usr/bin/env python
# -*- coding: utf-8 -*-

from pkg_resources import resource_filename


class PathCreator:
    def __init__(self):
        pass

    @staticmethod
    def create_image_path(resource_name=''):
        return resource_filename('images', resource_name)


if __name__ == "__main__":
    pc = PathCreator()
    print pc.create_image_path('exit.png')
